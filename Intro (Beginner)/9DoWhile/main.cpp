
#include <iostream> // this is importing library 
#include <iomanip> // cout string formating
// #include <limits> // library for numeric limits
#include <string>
// this is used so that we will not need to type std::function for all std function
using namespace std; 

int main()
{   
    int actualPin = 1234, pin, errorCount = 0;
    do {
        cout << "PIN: ";
        cin >> pin;
        // if pin is incorrect add errorCount
        errorCount+=(pin != actualPin);
    } while((errorCount<3) && (pin!=actualPin));

    if (errorCount < 3){
        cout << "Loading ... " << endl;
    } else {
        cout << "Blocked ... " << endl;
    }


    system("pause>0");
    return 0;
}



