
#include <iostream> // this is importing library 
#include <iomanip> // cout string formating
// #include <limits> // library for numeric limits
#include <list>
#include <string>
// this is used so that we will not need to type std::function for all std function
using namespace std; 


int main()
{   
    int rows, cols;
    cout << "Please input number of rows and columns of Matrix: " << endl ;
    cin >> rows >> cols;
    // Allocate the 2d Matrix
    int** matrix = new int*[rows];
    for (int i=0;i<rows;i++){
        matrix[i] = new int[cols];
    }
    matrix[0][0] = 99999;

    // De-allocate the 2d Matrix
    for (int i=0;i<rows;i++){
        delete[] matrix[i];
    }
    delete[] matrix;
    matrix = NULL;

    system("pause>0");
    return 0;
}



