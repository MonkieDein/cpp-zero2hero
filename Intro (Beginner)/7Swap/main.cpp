
#include <iostream> // this is importing library 
#include <iomanip> // cout string formating
// #include <limits> // library for numeric limits
#include <string>
// this is used so that we will not need to type std::function for all std function
using namespace std; 

// Extra Variable Swapping
void swap1(int* pt_a, int* pt_b){
    int c = *pt_a;
    *pt_a = *pt_b;
    *pt_b = c;
}

// Mathematical Swapping 
void swap2(int* pt_a, int* pt_b){
    *pt_a = (*pt_a) + (*pt_b) ;
    *pt_b = (*pt_a) - (*pt_b) ;
    *pt_a = (*pt_a) - (*pt_b) ;
}

// Xor Swapping 
void swap3(int* pt_a, int* pt_b){
    *pt_a = (*pt_a) ^ (*pt_b) ;
    *pt_b = (*pt_a) ^ (*pt_b) ;
    *pt_a = (*pt_a) ^ (*pt_b) ;
}


int main()
{   
    int a = 10, b = 20;
    cout << a << string(10,' ') << b << endl; 
    swap1(&a,&b);
    cout << a << string(10,' ') << b << endl;
    swap2(&a,&b); 
    cout << a << string(10,' ') << b << endl;
    swap3(&a,&b); 
    cout << a << string(10,' ') << b << endl;

    system("pause>0");
    return 0;
}



