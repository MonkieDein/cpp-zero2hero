
#include <iostream> // this is importing library 
#include <iomanip> // cout string formating
// #include <limits> // library for numeric limits
#include <string>
// this is used so that we will not need to type std::function for all std function
using namespace std; 

int main()
{   
    cout << string(30,'-') << endl;

    cout << "5 + 2 = " << 5 + 2 << endl;
    cout << "5 / 2 = " << 5 / 2 << endl;
    cout << "5.0 / 2 = " << 5.0/2 << endl;
    cout << "5 % 2 = " << 5 % 2 << endl;

    cout << string(30,'-') << endl;

    // unary operator  ++ , -- 
    int counter  = 7;

    cout << "During 7++ \t" << counter++ << endl; // increment after the line.
    cout << "After 7++ \t" << counter << endl; // print next line.
    
    cout << "During --8 \t" << --counter << endl; // decrenebt before the line.
    cout << "After --8 \t" << counter << endl; // print next line.
    
    cout << string(30,'-') << endl;
    
    // comparison operator < , > , <= , >= , == , !=
    int a = 5, b = 8;
    cout << "a = 5 \t\t b = 8" << endl << endl;
    string s = (a != b)? "True" : "False"; // Inline if statement
    cout << "a != b ? " << s << endl;

    s = (a == 5 || b == 5)? "True" : "False";
    cout << "(a == 5 || b == 5) ? " << s << endl;

    s = (a == 5 && b == 5)? "True" : "False";
    cout << "(a == 5 && b == 5) ? " << s << endl;

    s = (a == 5 && b == 5+3)? "True" : "False";
    cout << "(a == 5 && b == 5+3) ? " << s << endl;


    system("pause>0");
    return 0;
}
