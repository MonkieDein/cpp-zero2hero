
#include <iostream> // this is importing library 
#include <iomanip> // cout string formating
// #include <limits> // library for numeric limits
#include <string>
// this is used so that we will not need to type std::function for all std function
using namespace std; 

int main()
{   
    float n1, n2;
    char operation;
    cout << "Type a simple equation : A ( ) B " << endl;
    cin >> n1 >> operation >> n2;
    switch (operation)
    {
    case '+':
        cout << n1 << operation << n2 << " = " << n1 + n2 << endl;
        break;
    case '-':
        cout << n1 << operation << n2 << " = " << n1 - n2 << endl;
        break;
    case '*':
        cout << n1 << operation << n2 << " = " << n1 * n2 << endl;
        break;
    case '/':
        cout << n1 << operation << n2 << " = " << n1 / n2 << endl;
        break;
    case '%':
        if ((int(n1)-n1 == 0) && (int(n2)-n2==0)){
            cout << n1 << operation << n2 << " = " << int(n1) + int(n2) << endl;
        } else{
            cout << "Modulo (%) operator only valid for integer value" << endl;
        }
        break;
    default: 
        cout << operation << " is not a valid operation" << endl;
    }


    system("pause>0");
    return 0;
}



