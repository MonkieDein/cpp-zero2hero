
#include <iostream> // this is importing library 
#include <iomanip> // cout string formating
// #include <limits> // library for numeric limits
#include <list>
#include <string>
// this is used so that we will not need to type std::function for all std function
using namespace std; 

class YouTubeChannel{
// Private variable is created so other people cannot change variable 
// without using the function or method in the object
private:
    string Name;
    int SubscribersCount;
    list<string> PublishedVideoTitles;
// Protected Variable can be seen for other class that inherit this class.
protected:
    string OwnerName;
    int ContentQuality;
// Public Variable or function is visible to everyone who use the object.
public:
    // constructor when create YT channel
    YouTubeChannel(string name, string ownerName){
        Name = name;
        OwnerName= ownerName;
        SubscribersCount = 0;
        ContentQuality = 0;
    }
    void GetInfo(){
        cout << string(20,'-') <<endl;
        cout << "Name: " << Name << endl;
        cout << "Owner: " << OwnerName << endl;
        cout << "Subs: " << SubscribersCount << endl;
        cout << "Videos: "<< endl;
        for(string video : PublishedVideoTitles){
            cout <<"\t" << video << endl;
        }
        cout << string(20,'-') <<endl;
    }
    void Subscribe(){ SubscribersCount++; }
    void Unsubscribe(){ SubscribersCount--; }
    void PublishVideo(string video){
        PublishedVideoTitles.push_back(video);
    }
    void CheckAnalytics(){
        if (ContentQuality < 5){
            cout << Name << " is not update frequently" << endl;
        } else {
            cout << Name << " is updated frequently" << endl;
        }
    }
};

// Class Inheritence
class SubChannel: public YouTubeChannel {
public:
    SubChannel(string name, string ownerName):YouTubeChannel(name, ownerName){ 
    }
    void UpdateChannel(){
        cout << OwnerName << "update his/her SubChannel" << endl ;
        ContentQuality ++;
    }
};

// Class Inheritence
class MainChannel: public YouTubeChannel {
public:
    MainChannel(string name, string ownerName):YouTubeChannel(name, ownerName){ 
    }
    void UpdateChannel(){
        cout << OwnerName << "update his/her MainChannel" << endl ;
        ContentQuality ++;
    }
};

int main()
{   

    SubChannel YT("MonkieStudy","Monkie");
    YT.UpdateChannel();
    YT.PublishVideo("Study AT Diamond Lib");
    YT.UpdateChannel();
    YT.UpdateChannel();
    YT.UpdateChannel();
    YT.PublishVideo("My Ipad");
    YT.UpdateChannel();
    YT.UpdateChannel();
    YT.PublishVideo("My Phd");
    YT.UpdateChannel();
    YT.Subscribe();
    YT.Subscribe();
    YT.Subscribe();
    YT.Subscribe();
    YT.Subscribe();
    YT.GetInfo();
    YT.CheckAnalytics();




    MainChannel YT2("MonkieMain","Monkie");
    YT2.PublishVideo("Playing League");
    YT2.UpdateChannel();
    YT2.Subscribe();
    YT2.GetInfo();
    YT2.CheckAnalytics();

    system("pause>0");
    return 0;
}



