
#include <iostream> // this is importing library 
// this is used so that we will not need to type std::function for all std function
using namespace std; 

int main()
{
    // 1st rule : Use meaningful variable name
    cout << "Pleas enter your Annual Salary"  << endl;
    float annualSalary;
    cin >> annualSalary;
    float monthlySalary = annualSalary/12;
    cout << "Your Monthly Salary is " << monthlySalary << endl;
    cout << "In 10 Years you will be earning " << 10 * annualSalary << endl;

    // 2nd rule : '_' is the only allowed sign in variable name, 
    //            and variable name cannot start with number (can have number);
    // not a variable : 8var , var test , val+test 
    // proper variable : var8, var_Test






    return 0;
}
