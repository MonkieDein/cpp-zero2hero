
#include <iostream> // this is importing library 
#include <iomanip> // cout string formating
// #include <limits> // library for numeric limits
#include <string>
// this is used so that we will not need to type std::function for all std function
using namespace std; 

int main()
{   
    // Type casting to output of calloc function (void *) -> (char *)
    string Message;

    cout << "Enter a message to be encrypted with ASCII" << endl;

    // cin >> Message;
    // Cin consider (whitespaces, tabs, new-line...) as terminating the value
    // Getline on another end will get the whole line before "Enter".
    getline(cin, Message);
    cout << " " ;
    for (int i=0;i<Message.size();i++){
        cout << setw(3) << int(Message[i]) << " ";
    }
    cout<< endl;

    for (int i=0;i<Message.size();i++){
        cout << setw(3) << Message[i] << " ";
    }
    cout<< endl;


    return 0;
}
