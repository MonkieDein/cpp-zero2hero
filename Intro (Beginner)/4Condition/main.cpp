
#include <iostream> // this is importing library 
#include <iomanip> // cout string formating
// #include <limits> // library for numeric limits
#include <string>
// this is used so that we will not need to type std::function for all std function
using namespace std; 

int main()
{   
    int UserInput = 0;
    // Ask for user input integer and repeat if User enter invalid input
    while (UserInput <= 0){
        cout << "Enter an positive integer" << endl;
        cin >> UserInput;
    }
    // Determine odd or even and print message out
    if (UserInput % 2 == 0){
        cout << UserInput << " is an even number." << endl;
    } else{
        cout << UserInput << " is an odd number." << endl;
    }


    system("pause>0");
    return 0;
}
