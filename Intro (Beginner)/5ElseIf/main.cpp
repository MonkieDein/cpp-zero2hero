
#include <iostream> // this is importing library 
#include <iomanip> // cout string formating
// #include <limits> // library for numeric limits
#include <string>
// this is used so that we will not need to type std::function for all std function
using namespace std; 
// Ask user to enter 3 values as the side length
// of a  triangle and determine if it's
// equilateral, isosceles or scalene troamge
int main()
{   
    float a , b , c ;
    cout << "Enter 3 values as the side length of a  triangle." << endl;
    cin >> a >> b >> c ;
    if ((a == b) && (a == c)){
        cout << "Your input triangle is Equlateral." << endl;
    } else if ((a != b) && (a != c) && (b != c)){
        cout << "Your input triangle is Scalene." << endl; 
    } else {
        cout << "Your input triangle is Isosceles." << endl;
    }
    system("pause>0");
    return 0;
}
