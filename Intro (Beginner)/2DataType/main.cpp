
#include <iostream> // this is importing library 
#include <iomanip> // cout string formating
#include <limits> // library for numeric limits

// this is used so that we will not need to type std::function for all std function
using namespace std; 

int main()
{   
    // commonly used variable type
    int yearOfBirth = 1995;
    char gender = 'f';
    bool isOlderThan18 = true;
    float averageGrade = 4.5;
    double balance = 123413241234;

    // find how many bytes each variable use
    // we can use setw to allign our printing
    cout << string(73,'-') << endl;
    cout << left; // left allignment
    // print header
    cout << "| " << setw(16) << "VariableType" << "| " << setw(16) << "Size (Byte)" << "| " <<
             setw(16) << "Minimum" << "| " << setw(16) << "Maximum" << "| " << endl;
    // print elements
    cout << "| " << setw(16) << "int" << "| " << setw(16) << sizeof(int) << "| " <<
             setw(16) << INT_MIN << "| " << setw(16) << INT_MAX << "| " << endl;
    cout << "| " << setw(16) << "char" << "| " << setw(16) << sizeof(char) << "| " <<
             setw(16) << CHAR_MIN << "| " << setw(16) << CHAR_MAX << "| " << endl;
    cout << "| " << setw(16) << "boolean" << "| " << setw(16) << sizeof(bool) << "| " <<
             setw(16) << "false" << "| " << setw(16) << "true" << "| " << endl;
    cout << "| " << setw(16) << "float" << "| " << setw(16) << sizeof(float) << "| " <<
             setw(16) << -numeric_limits<float>::infinity() << "| " << setw(16) << numeric_limits<float>::infinity() << "| " << endl;
    cout << "| " << setw(16) << "double" << "| " << setw(16) << sizeof(double) << "| " <<
             setw(16) << -numeric_limits<double>::infinity() << "| " << setw(16) << numeric_limits<double>::infinity() << "| " << endl;
    cout << string(73,'-') << endl;
    return 0;
}
