// The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: (you may want to display this pattern in a fixed font for better legibility)
// P   A   H   N
// A P L S I I G
// Y   I   R
// And then read line by line: "PAHNAPLSIIGYIR"

// Write the code that will take a string and make this conversion given a number of rows:
// string convert(string s, int numRows); 

// Example 1:
// Input: s = "PAYPALISHIRING", numRows = 3
// Output: "PAHNAPLSIIGYIR"

// Example 2:
// Input: s = "PAYPALISHIRING", numRows = 4
// Output: "PINALSIGYAHRPI"
// Explanation:
// P     I    N
// A   L S  I G
// Y A   H R
// P     I

// Example 3:
// Input: s = "A", numRows = 1
// Output: "A"

// Constraints:
// 1 <= s.length <= 1000
// s consists of English letters (lower-case and upper-case), ',' and '.'.
// 1 <= numRows <= 1000

string convert(string s, int numRows) {
    int n = s.size();
    if ((n <= numRows)||(numRows == 1)){
        return(s);
    }
    string b = s;
    int rep = (numRows-1)*2;
    int cnt = 0;
    for (int r = 0;r<numRows;r++){
        int index = r;
        while (index < n){
            b[cnt++] = s[index];
            if ((r != 0)&&(r != (numRows-1))&&(index+rep-2*r < n)){
                b[cnt++] = s[index+rep-2*r];
            }
            index = index + rep;
        }
    }
    return(b);
}


