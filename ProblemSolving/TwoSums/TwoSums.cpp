// Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
// You may assume that each input would have exactly one solution, and you may not use the same element twice.
// You can return the answer in any order.
// 
// Example1:
// Input: nums = [2,7,11,15], target = 9
// Output: [0,1]
// Output: Because nums[0] + nums[1] == 9, we return [0, 1].
// 
// Example 2:
// Input: nums = [3,2,4], target = 6
// Output: [1,2]
// 
// Example 3:
// Input: nums = [3,3], target = 6
// Output: [0,1]
// 
// Only one valid answer exists.
// 

#include<iostream>
#include<vector>
#include<map>

using namespace std;


class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        // create map
        map<int,int> m;
        // go through vector if the target-value is in map we return i and the map index
        // otherwise add the value into the map.
        for(int i=0;i<nums.size();i++){
            auto it = m.find(target-nums[i]);
            int found = (it != m.end()? it->second : -1);
            
            if (found!=-1){
                return(vector({found,i}));
            } else {
                m.insert(make_pair(nums[i],i));
            }
        }
        return(vector({-1,-1}));
    }
};

