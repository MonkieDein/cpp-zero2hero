// Given an integer x, return true if x is palindrome integer.
// An integer is a palindrome when it reads the same backward as forward. For example, 121 is palindrome while 123 is not.
// 
// Example 1:
// Input: x = 121
// Output: true
// 
// Example 2:
// Input: x = -121
// Output: false
// Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.
// 
// Example 3:
// Input: x = 10
// Output: false
// Explanation: Reads 01 from right to left. Therefore it is not a palindrome.
// 
// Example 4:
// Input: x = -101
// Output: false

bool isPalindrome(int x) {
    // if exist negative sign then sure not palindrome
    if (x<0){
        return 0;
    }
    // remember original_x
    int ori=x;
    // new_x = flipped_x 
    long new_x = 0;
    while (x!=0){
        new_x = new_x*10 + x%10;
        x = x/10;
    }
    // compare them
    return ori==new_x;
}
