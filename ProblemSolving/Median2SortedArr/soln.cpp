// Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.

// The overall run time complexity should be O(log (m+n)). 

// Example 1:
// Input: nums1 = [1,3], nums2 = [2]
// Output: 2.00000
// Explanation: merged array = [1,2,3] and median is 2.

// Example 2:
// Input: nums1 = [1,2], nums2 = [3,4]
// Output: 2.50000
// Explanation: merged array = [1,2,3,4] and median is (2 + 3) / 2 = 2.5.

// Example 3:
// Input: nums1 = [0,0], nums2 = [0,0]
// Output: 0.00000

// Example 4:
// Input: nums1 = [], nums2 = [1]
// Output: 1.00000

// Example 5:
// Input: nums1 = [2], nums2 = []
// Output: 2.00000
 
// Constraints:
// nums1.length == m
// nums2.length == n
// 0 <= m <= 1000
// 0 <= n <= 1000
// 1 <= m + n <= 2000
// -106 <= nums1[i], nums2[i] <= 106



#include <limits> // library for numeric limits

// Check if an integer is odd number
bool isOdd(int i){
    return(i%2==1);
}

double Median(vector<int>& nums){
    if (isOdd(nums.size())){
        return(nums[nums.size()/2]);
    } else {
        return((nums[nums.size()/2]+nums[(nums.size()/2)-1])*0.5);
    }
}

double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
    int S1 = nums1.size();
    int S2 = nums2.size();
    
    if (S1 > S2){return(findMedianSortedArrays(nums2,nums1));}
    if (S1 == 0){return(Median(nums2));}
    
    int Sum = S1 + S2;
    int V1_begin = 0;
    int V1_end = S1;
    
    // define variable with arbitrary number that allow the while loop to run
    int L1 = INT_MAX,L2 = INT_MAX;
    int R1 = INT_MIN,R2 = INT_MIN;
    
    // Binary Search a cutting location such that Left1 <= Right2 and Right1 <= Left2
    // While Left1.size() + Left2.size() = Right1.size() + Right2.size()
    while (!((L1 <= R2) && (L2 <= R1))){
        int cut1 = (V1_begin + V1_end)/2;
            // If odd TotalSize, default Lhs has the extra 1.
        int cut2 = (Sum + 1)/2 - cut1;
    
        L1 = (cut1 == 0)? INT_MIN : nums1[cut1 - 1];
        R1 = (cut1 == S1)? INT_MAX : nums1[cut1];

        L2 = (cut2 == 0)? INT_MIN : nums2[cut2 - 1];
        R2 = (cut2 == S2)? INT_MAX : nums2[cut2];
        
        if (L1 > R2){
            V1_end = cut1 - 1;
        } else if (L2 > R1){
            V1_begin = cut1 + 1;
        }
    }
    if (isOdd(Sum)){
        return(max(L1,L2));
    } else {
        return((max(L1,L2)+min(R1,R2))*0.5);
    }     
    return(-1); //  should never reached here.
        
}
