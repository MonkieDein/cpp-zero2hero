// Given a string s, find the length of the longest substring without repeating characters.

// Example 1:
// Input: s = "abcabcbb"
// Output: 3
// Explanation: The answer is "abc", with the length of 3.

// Example 2:
// Input: s = "bbbbb"
// Output: 1
// Explanation: The answer is "b", with the length of 1.

// Example 3:
// Input: s = "pwwkew"
// Output: 3
// Explanation: The answer is "wke", with the length of 3.
// Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.

// Example 4:
// Input: s = ""
// Output: 0
 

// Constraints:
// 0 <= s.length <= 5 * 104
// s consists of English letters, digits, symbols and spaces.


#include <set>
#include <map>
#include <iostream> // this is cout and cin library 
#include <string>
using namespace std;

int lengthOfLongestSubstring(string s) {
    map<char,int> m;
    int i=0;
    int n=0;
    int s_n = s.size();
    
    while ((i+n)<s_n){
        
        // keep track of character counts
        auto it = m.find(s[i+n]);
        int found = (it != m.end()? it->second : -1);
        // create key if not exit
        if (found == -1){
            m[s[i+n]] = 1;
        } else {
            m[s[i+n]]++ ;
        }
        
        // added window
        if (m.size() > n){
            n++;
        } else { 
            // shifting window and remove key that has no element
            if ((--m[s[i]])==0){
                m.erase(s[i]);
            }
            i++;
        }
    }
    return n;
}


int main(){
    string s = "pwwkew";
    cout << s << endl;
    cout << "Output :" << lengthOfLongestSubstring(s) << endl;
    system("pause>0");

    return(1);
}





