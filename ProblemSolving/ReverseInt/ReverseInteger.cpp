// Given a signed 32-bit integer x, return x with its digits reversed. If reversing x causes the value to go outside the signed 32-bit integer range [-231, 231 - 1], then return 0.
// 
// Assume the environment does not allow you to store 64-bit integers (signed or unsigned).
// 
// Example 1:
// Input: x = 123
// Output: 321
// 
// Example 2:
// Input: x = -123
// Output: -321
// 
// Example 3:
// Input: x = 120
// Output: 21
// 
// Example 4:
// Input: x = 0
// Output: 0



int fit_INT(long x){
    if (x<INT_MIN  || x>INT_MAX){
        return(0);
    } else {
        return(int(x));
    }
}

int reverse(int x) {
    long rev_x = 0;
    while (x/10!=0){
        rev_x = rev_x*10 + x%10;
        x = x/10;
    }
    rev_x = rev_x*10 + x%10; 
    return(fit_INT(rev_x));
}