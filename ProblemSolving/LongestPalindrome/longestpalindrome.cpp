// Given a string s, return the longest palindromic substring in s.

// Example 1:
// Input: s = "babad"
// Output: "bab"
// Note: "aba" is also a valid answer.

// Example 2:
// Input: s = "cbbd"
// Output: "bb"

// Example 3:
// Input: s = "a"
// Output: "a"

// Example 4:
// Input: s = "ac"
// Output: "a"
 
// Constraints:
// 1 <= s.length <= 1000
// s consist of only digits and English letters (lower-case and/or upper-case),

// treat index i as center as find the palindrome lenght.
int PalindromCompare(string s,int i,int n){
    int extra=1;
    int first=0;
    if(s[i] == s[i+1]){
        while ((i-extra >= 0)&&(i+extra<n)&&(s[i-extra] == s[i+1+extra])){
            extra++;
        }
        first = extra*2;
    } 

    extra=1;
    while((i-extra >= 0)&&(i+extra<n)&&(s[i-extra] == s[i+extra])){
        extra++;
    }
    return(max(first,(extra*2-1)));
}
// brute force: for ever center find calculate the palindrom length. find best.
string longestPalindrome(string s) {
    int n = s.size();
    int best = 0;
    int best_i = 0;
    for (int i=0;i<n;i++){
        int x = PalindromCompare(s,i,n);
        best_i = best>=x? best_i: i;
        best = best>=x? best: x;
    }
    if (best%2==1){
        return(s.substr(best_i-best/2,best));
    } else {
        return(s.substr(best_i-best/2+1,best));
    }
}