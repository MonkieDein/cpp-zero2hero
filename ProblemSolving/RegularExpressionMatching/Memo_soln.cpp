// Compared to the easy solution, the only difference is the memo table. 
// Everytime before we return the solution, we pass the solution to the 
// memo table so that we don't have to solve a problem that has been solved before.

#include <string>
using namespace std;

bool equal(char a, char b){
    return(a==b || b=='.');
}

int memo[20][30];
bool begin = 1;

bool isMatch(string s, string p) {
    // initialize memo
    if (begin){
        fill( &memo[0][0], &memo[0][0] + sizeof(memo)/sizeof(memo[0][0]), -1 );
        begin = 0;
    }
    int s_n = s.size();
    int p_n = p.size();
    
    if (memo[s_n][p_n]!=-1){
        return memo[s_n][p_n];
    }

    int j=0;
    // Declare Dynamic Array for Memoization
    for (int i=0;i<p_n;i++){
        if (p[i+1]=='*'){
            // Use the star
            if (s_n-j-1>=0 && p_n-i>=0 && equal(s[j],p[i])){
                memo[s_n-j-1][p_n-i] = isMatch(s.substr(j+1,s_n-j-1),p.substr(i,p_n-i));
                if (memo[s_n-j-1][p_n-i]){
                    return 1;
                }
            }
            // Don't use the star
            i++;
        } else {
            if (j>=s_n || !equal(s[j],p[i])){
                memo[s_n][p_n] = 0;
                return 0;
            }
            j++;
        }
    }
    memo[s_n][p_n] = (j == s_n);
    return(j == s_n);
}

