// Given an input string (s) and a pattern (p), implement regular expression matching with support for '.' and '*' where: 
// 
// '.' Matches any single character.​​​​
// '*' Matches zero or more of the preceding element.
// The matching should cover the entire input string (not partial).
// 
// Example 1:
// Input: s = "aa", p = "a"
// Output: false
// Explanation: "a" does not match the entire string "aa".
// 
// Example 2:
// Input: s = "aa", p = "a*"
// Output: true
// Explanation: '*' means zero or more of the preceding element, 'a'. Therefore, by repeating 'a' once, it becomes "aa".
// 
// Example 3:
// Input: s = "ab", p = ".*"
// Output: true
// Explanation: ".*" means "zero or more (*) of any character (.)".

bool equal(char a, char b){
    return(a==b || b=='.');
}


bool isMatch(string s, string p) {
    int j=0;
    int p_n = p.size();
    int s_n = s.size();
    for (int i=0;i<p_n;i++){
        if (p[i+1]=='*'){
            // use star :  if use star once and repeat: if success, then success
            if (s_n-j-1>=0 && p_n-i>=0 && equal(s[j],p[i]) && isMatch(s.substr(j+1,s_n-j-1),p.substr(i,p_n-i))){
                return 1;
            }
            // don't use star
            i++;
        } else {
            if (j>=s_n || !equal(s[j],p[i])){
                return 0;
            }
            j++;
        }
    }
    return(j == s_n);

}